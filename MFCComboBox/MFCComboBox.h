
// MFCComboBox.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMFCComboBoxApp:
// このクラスの実装については、MFCComboBox.cpp を参照してください。
//

class CMFCComboBoxApp : public CWinApp
{
public:
	CMFCComboBoxApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CMFCComboBoxApp theApp;